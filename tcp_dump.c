#include<pcap.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<sys/socket.h>
#include<arpa/inet.h> 
#include<net/ethernet.h>
#include<netinet/ip_icmp.h>
#include<netinet/udp.h>
#include<netinet/tcp.h>
#include<netinet/ip.h>

void process_packet(u_char*, const struct pcap_packet_hdr*, const u_char*);
void process_ip_packet(const u_char*, int);
void print_ip_packet_to_file(const u_char*, int);
void print_tcp_packet_to_file(const u_char*, int);
void print_udp_packet_to_file(const u_char*, int);
void print_icmp_packet_to_file(const u_char*, int);
void PrintDataToFile(const u_char*, int);

FILE* log_file_txt;
struct sockaddr_in source, dest;
int tcp = 0, udp = 0, icmp = 0, others = 0, igmp = 0, total = 0, i, j;

int main()
{
	pcap_if_t* alldevicesp, *device;
	pcap_t* handle;

	char errbuf[100], * device_name, devices[100][100];
	int count = 1, n;

	//First get list of available devices
	printf("Processing ... Finding available devices ... ");
	if (pcap_findalldevices(&alldevicesp, errbuf))
	{
		printf("Error finding devices : %s", errbuf);
		exit(1);
	}
	printf("Done");

	//Print the available devices
	printf("\nAvailable Devices are :\n");
	for (device = alldevicesp; device != NULL; device = device->next)
	{
		printf("%d. %s - %s\n", count, device->name, device->description);
		if (device->name != NULL)
		{
			strcpy(devices[count], device->name);
		}
		count++;
	}

	//Ask user which device to sniff
	printf("Enter the number of the device you want to check : ");
	scanf("%d", &n);
	device_name = devices[n];

	//Open the device for sniffing
	printf("Opening device %s for checking ... ", device_name);
	handle = pcap_open_live(device_name, 65536, 1, 0, errbuf);

	if (handle == NULL)
	{
		fprintf(stderr, "Couldn't open device %s : %s\n", device_name, errbuf);
		exit(1);
	}
	printf("Done\n");

	log_file_txt = fopen("log.txt", "w");
	if (log_file_txt == NULL)
	{
		printf("Error! Unable to create file.");
	}

	//Put the device in sniff loop
	pcap_loop(handle, -1, process_packet, NULL);

	return 0;
}

void process_packet(u_char* args, const struct pcap_packet_hdr* header, const u_char* buffer)
{
	int size = header->len;

	//Get the IP Header part of this packet , excluding the ethernet header
	struct iphdr* iph = (struct iphdr*)(buffer + sizeof(struct ethhdr));
	++total;
	switch (iph->protocol) //Check the Protocol and do accordingly...
	{
	case 1:  //ICMP Protocol id
		++icmp;
		print_icmp_packet_to_file(buffer, size);
		break;

	case 2:  //IGMP Protocol id
		++igmp;
		break;

	case 6:  //TCP Protocol id
		++tcp;
		print_tcp_packet_to_file(buffer, size);
		break;

	case 17: //UDP Protocol id
		++udp;
		print_udp_packet_to_file(buffer, size);
		break;

	default: //Some Other Protocol like ARP etc...
		++others;
		break;
	}
	printf("TCP : %d   UDP : %d   ICMP : %d   IGMP : %d   Others : %d   Total : %d\r", tcp, udp, icmp, igmp, others, total);
}

void print_ethernet_header_to_file(const u_char* Buffer, int Size)
{
	struct ethhdr* eth = (struct ethhdr*)Buffer;

	fprintf(log_file_txt, "\nEthernet Header\n");
	fprintf(log_file_txt, "   |-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_dest[0], eth->h_dest[1], eth->h_dest[2], eth->h_dest[3], eth->h_dest[4], eth->h_dest[5]);
	fprintf(log_file_txt, "   |-Source Address      : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_source[0], eth->h_source[1], eth->h_source[2], eth->h_source[3], eth->h_source[4], eth->h_source[5]);
	fprintf(log_file_txt, "   |-Protocol            : %u \n", (unsigned short)eth->h_proto);
}

void print_ip_header_to_file(const u_char* Buffer, int Size)
{
	print_ethernet_header_to_file(Buffer, Size);

	unsigned short iphdrlen;

	struct iphdr* iph = (struct iphdr*)(Buffer + sizeof(struct ethhdr));
	iphdrlen = iph->ihl * 4;

	memset(&source, 0, sizeof(source));
	source.sin_addr.s_addr = iph->saddr;

	memset(&dest, 0, sizeof(dest));
	dest.sin_addr.s_addr = iph->daddr;

	fprintf(log_file_txt, "\nIP Header\n");
	fprintf(log_file_txt, "   |-IP Version        : %d\n", (unsigned int)iph->version);
	fprintf(log_file_txt, "   |-IP Header Length  : %d DWORDS or %d Bytes\n", (unsigned int)iph->ihl, ((unsigned int)(iph->ihl)) * 4);
	fprintf(log_file_txt, "   |-Type Of Service   : %d\n", (unsigned int)iph->tos);
	fprintf(log_file_txt, "   |-IP Total Length   : %d  Bytes(Size of Packet)\n", ntohs(iph->tot_len));
	fprintf(log_file_txt, "   |-Identification    : %d\n", ntohs(iph->id));
	fprintf(log_file_txt, "   |-TTL      : %d\n", (unsigned int)iph->ttl);
	fprintf(log_file_txt, "   |-Protocol : %d\n", (unsigned int)iph->protocol);
	fprintf(log_file_txt, "   |-Checksum : %d\n", ntohs(iph->check));
	fprintf(log_file_txt, "   |-Source IP        : %s\n", inet_ntoa(source.sin_addr));
	fprintf(log_file_txt, "   |-Destination IP   : %s\n", inet_ntoa(dest.sin_addr));
}

void print_tcp_packet_to_file(const u_char* Buffer, int Size)
{
	unsigned short iphdrlen;

	struct iphdr* iph = (struct iphdr*)(Buffer + sizeof(struct ethhdr));
	iphdrlen = iph->ihl * 4;

	struct tcphdr* tcph = (struct tcphdr*)(Buffer + iphdrlen + sizeof(struct ethhdr));

	int header_size = sizeof(struct ethhdr) + iphdrlen + tcph->doff * 4;

	fprintf(log_file_txt, "\n\n***********************TCP Packet*************************\n");

	print_ip_header_to_file(Buffer, Size);

	printf("|-TCP header |-Source Port : %u |-Destination Port : %u |-Checksum : %d\n", ntohs(tcph->source), ntohs(tcph->dest), ntohs(tcph->check));

	fprintf(log_file_txt, "\nTCP Header\n");
	fprintf(log_file_txt, "   |-Source Port      : %u\n", ntohs(tcph->source));
	fprintf(log_file_txt, "   |-Destination Port : %u\n", ntohs(tcph->dest));
	fprintf(log_file_txt, "   |-Sequence Number    : %u\n", ntohl(tcph->seq));
	fprintf(log_file_txt, "   |-Acknowledge Number : %u\n", ntohl(tcph->ack_seq));
	fprintf(log_file_txt, "   |-Header Length      : %d DWORDS or %d BYTES\n", (unsigned int)tcph->doff, (unsigned int)tcph->doff * 4);
	fprintf(log_file_txt, "   |-Urgent Flag          : %d\n", (unsigned int)tcph->urg);
	fprintf(log_file_txt, "   |-Acknowledgement Flag : %d\n", (unsigned int)tcph->ack);
	fprintf(log_file_txt, "   |-Push Flag            : %d\n", (unsigned int)tcph->psh);
	fprintf(log_file_txt, "   |-Reset Flag           : %d\n", (unsigned int)tcph->rst);
	fprintf(log_file_txt, "   |-Synchronise Flag     : %d\n", (unsigned int)tcph->syn);
	fprintf(log_file_txt, "   |-Finish Flag          : %d\n", (unsigned int)tcph->fin);
	fprintf(log_file_txt, "   |-Window         : %d\n", ntohs(tcph->window));
	fprintf(log_file_txt, "   |-Checksum       : %d\n", ntohs(tcph->check));
	fprintf(log_file_txt, "   |-Urgent Pointer : %d\n", tcph->urg_ptr);
	fprintf(log_file_txt, "\n");
	fprintf(log_file_txt, "                        DATA Dump                         ");
	fprintf(log_file_txt, "\n");

	fprintf(log_file_txt, "IP Header\n");
	PrintDataToFile(Buffer, iphdrlen);

	fprintf(log_file_txt, "TCP Header\n");
	PrintDataToFile(Buffer + iphdrlen, tcph->doff * 4);

	fprintf(log_file_txt, "Data Payload\n");
	PrintDataToFile(Buffer + header_size, Size - header_size);

	fprintf(log_file_txt, "\n###########################################################");
}

void print_udp_packet_to_file(const u_char* Buffer, int Size)
{

	unsigned short iphdrlen;

	struct iphdr* iph = (struct iphdr*)(Buffer + sizeof(struct ethhdr));
	iphdrlen = iph->ihl * 4;

	struct udphdr* udph = (struct udphdr*)(Buffer + iphdrlen + sizeof(struct ethhdr));

	int header_size = sizeof(struct ethhdr) + iphdrlen + sizeof udph;

	fprintf(log_file_txt, "\n\n***********************UDP Packet*************************\n");

	print_ip_header_to_file(Buffer, Size);

	printf("|-UDP header |-Source Port : %d |-Destination Port : %d |-UDP Length : %d |-UDP Checksum : %d\n", ntohs(udph->source), ntohs(udph->dest), ntohs(udph->len), ntohs(udph->check));

	fprintf(log_file_txt, "\nUDP Header\n");
	fprintf(log_file_txt, "   |-Source Port      : %d\n", ntohs(udph->source));
	fprintf(log_file_txt, "   |-Destination Port : %d\n", ntohs(udph->dest));
	fprintf(log_file_txt, "   |-UDP Length       : %d\n", ntohs(udph->len));
	fprintf(log_file_txt, "   |-UDP Checksum     : %d\n", ntohs(udph->check));

	fprintf(log_file_txt, "\nIP Header\n");
	PrintDataToFile(Buffer, iphdrlen);

	fprintf(log_file_txt, "UDP Header\n");
	PrintDataToFile(Buffer + iphdrlen, sizeof udph);

	fprintf(log_file_txt, "Data Payload\n");

	//Move the pointer ahead and reduce the size of string
	PrintDataToFile(Buffer + header_size, Size - header_size);

	fprintf(log_file_txt, "\n###########################################################");
}

void print_icmp_packet_to_file(const u_char* Buffer, int Size)
{
	unsigned short iphdrlen;

	struct iphdr* iph = (struct iphdr*)(Buffer + sizeof(struct ethhdr));
	iphdrlen = iph->ihl * 4;

	struct icmphdr* icmph = (struct icmphdr*)(Buffer + iphdrlen + sizeof(struct ethhdr));

	int header_size = sizeof(struct ethhdr) + iphdrlen + sizeof icmph;

	fprintf(log_file_txt, "\n\n***********************ICMP Packet*************************\n");

	print_ip_header_to_file(Buffer, Size);

	fprintf(log_file_txt, "\nICMP Header\n");
	fprintf(log_file_txt, "   |-Type : %d", (unsigned int)(icmph->type));

	if ((unsigned int)(icmph->type) == 11)
	{
		fprintf(log_file_txt, "  (TTL Expired)\n");
	}
	else if ((unsigned int)(icmph->type) == ICMP_ECHOREPLY)
	{
		fprintf(log_file_txt, "  (ICMP Echo Reply)\n");
	}

	fprintf(log_file_txt, "   |-Code : %d\n", (unsigned int)(icmph->code));
	fprintf(log_file_txt, "   |-Checksum : %d\n", ntohs(icmph->checksum));

	fprintf(log_file_txt, "\nIP Header\n");
	PrintDataToFile(Buffer, iphdrlen);

	fprintf(log_file_txt, "UDP Header\n");
	PrintDataToFile(Buffer + iphdrlen, sizeof icmph);

	fprintf(log_file_txt, "Data Payload\n");

	PrintDataToFile(Buffer + header_size, (Size - header_size));

	fprintf(log_file_txt, "\n###########################################################");
}

void PrintDataToFile(const u_char* data, int Size)
{
	int i, j;
	for (i = 0; i < Size; i++)
	{
		if (i != 0 && i % 16 == 0)   //if one line of hex printing is complete...
		{
			fprintf(log_file_txt, "         ");
			for (j = i - 16; j < i; j++)
			{
				if (data[j] >= 32 && data[j] <= 128)
					fprintf(log_file_txt, "%c", (unsigned char)data[j]); //if its a number or alphabet

				else fprintf(log_file_txt, "."); //otherwise print a dot
			}
			fprintf(log_file_txt, "\n");
		}

		if (i % 16 == 0) fprintf(log_file_txt, "   ");
		fprintf(log_file_txt, " %02X", (unsigned int)data[i]);

		if (i == Size - 1)  //print the last spaces
		{
			for (j = 0; j < 15 - i % 16; j++)
			{
				fprintf(log_file_txt, "   "); // some spaces
			}

			fprintf(log_file_txt, "         ");

			for (j = i - i % 16; j <= i; j++)
			{
				if (data[j] >= 32 && data[j] <= 128)
				{
					fprintf(log_file_txt, "%c", (unsigned char)data[j]);
				}
				else
				{
					fprintf(log_file_txt, ".");
				}
			}

			fprintf(log_file_txt, "\n");
		}
	}
}