# tcpdump


to build project use this command:
`sudo gcc tcp_dump.c -lpcap`
`sudo ./a.out`

Objective: using C, analog of tcpdump (console application) which analyzes all packages that meet the specified criteria 
(like source/ destination MAC, source/ destination IP address, source/destination port), and keeps the following statistics:
• List of all MAC addresses of the packages; the number of packages sent to/from a specified MAC address.
• List of all IP addresses of the packages; quantity of packages sent via 4L protocols.